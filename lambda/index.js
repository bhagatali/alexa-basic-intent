const Alexa = require('alexa-sdk');
const constants = require('./constants/constants');
const onboardingStateHandlers = require('./handlers/onboardingStateHandler');
const mainStateHandlers = require('./handlers/mainStateHandler');

exports.handler = function(event, context, callback){
    const alexa = Alexa.handler(event, context, callback);
    alexa.appId = constants.appId;

    // This is how we hook up a DynamoDB table to Alexa. Alexa SDK takes care of the DB creation.
    // Just ensure your Lambda has permission to the DynamoDB.
    alexa.dynamoDBTableName = constants.dynamoDBTableName;
    alexa.registerHandlers(onboardingStateHandlers, mainStateHandlers);
    alexa.execute();
};