var constants = Object.freeze({
    appId : '',
    dynamoDBTableName: 'meetupSkillInformation',
    states: {
        ONBOARDING: '',
        MAIN: '_MAIN'
    }
});

module.exports = constants;