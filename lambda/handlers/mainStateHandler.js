var Alexa = require('alexa-sdk');
var constants = require('../constants/constants');
const alexaMeetups = require('../data/alexaMeetup');
const convertArrayToReadableString = require('../helpers/convertArrayToReadableString');

var mainStateHandlers = Alexa.CreateStateHandler(constants.states.MAIN, {
    'LaunchRequest' : function () {
        // Get user name from session - Every amazon Alexa has a unique but anonymous id. By hooking up a DynamoDB table, the Alexa SDK
        // saves the user information and any session information into the DynamoDB table. When a new session is invoked
        // the Alexa SDK looks for a row in the DynamoDB for that specific alexa user account. If it finds a record
        // in the DynamoDB table, it will pull all session information from that DynamoDB table.
        var userName = this.attributes[`userName`];

        if (userName) {
            this.emit(':ask', `Welcome back ${userName}! You can ask me about the various alexa meetups around the world, or listen to the alexa dev chat podcast.`, `What would you like to do?`);
        } else {
            this.handler.state = constants.states.ONBOARDING;
            this.emitWithState('NewSession');
        }
    },

    'AlexaMeetupNumbers': function () {
        const meetupNumbers = alexaMeetups.length;
        this.emit(':ask', `There are currently ${meetupNumbers} Alexa developer meetups. Check to see if your city is one of them!`, 'How can i help?');
    },

    'AlexaMeetupCity': function () {
        // Get Slot Values
        var USCitySlot = this.event.request.intent.slots.USCity.value;
        var EuropeanCitySlot = this.event.request.intent.slots.EuropeanCity.value;

        console.log(`US City: ${USCitySlot}`);
        console.log(`European City: ${EuropeanCitySlot}`);

        // Get City
        var city;
        if (USCitySlot) {
            city = USCitySlot;
        } else if (EuropeanCitySlot) {
            city = EuropeanCitySlot;
        } else {
            this.emit(':ask', 'Sorry, I didn\'t recognise that city name.', 'How can i help?');
        }

        // Check for City
        var cityMatch = '';
        for (var i = 0; i < alexaMeetups.length; i++) {
            if (alexaMeetups[i].city.toLowerCase() === city.toLowerCase()) {
                cityMatch = alexaMeetups[i].city;
            }
        }

        // London Check
        var londonCheck = ``;
        if (city.toLowerCase() === 'london') {
            londonCheck = `<audio src="https://s3.amazonaws.com/alexa.resources/Friends+-+London+Baby!.mp3" />`
        }

        // Respond to User
        if (cityMatch !== '') {
            this.emit(':ask', `${londonCheck} Yes! ${city} has an Alexa developer meetup!`, 'How can i help?');
        } else {
            this.emit(':ask', `Sorry, looks like ${city} doesn't have an Alexa developer meetup yet - why don't you start one!`, 'How can i help?');
            // this.emit(':ask', `You say, <phoneme alphabet="ipa" ph="pɪˈkɑːn">pecan</phoneme>. I say, <phoneme alphabet="ipa" ph="ghariyã">ghariyaan</phoneme>.`, 'How can i help?');
        }
    },

    'AlexaMeetupOrganisers': function () {
        // Get Slot Values
        var USCitySlot = this.event.request.intent.slots.USCity.value;
        var EuropeanCitySlot = this.event.request.intent.slots.EuropeanCity.value;

        // Get City
        var city;
        if (USCitySlot) {
            city = USCitySlot;
        } else if (EuropeanCitySlot) {
            city = EuropeanCitySlot;
        } else {
            this.emit(':ask', 'Sorry, I didn\'t recognise that city name.', 'How can i help?');
        }

        // Check for City
        var cityMatch = '';
        var cityOrganisers = '';
        for (var i = 0; i < alexaMeetups.length; i++) {
            if (alexaMeetups[i].city.toLowerCase() === city.toLowerCase()) {
                cityMatch = alexaMeetups[i].city;
                cityOrganisers = alexaMeetups[i].organisers;
            }
        }

        // London Check
        var londonCheck = ``;
        if (city.toLowerCase() === 'london') {
            londonCheck = `<audio src="https://s3.amazonaws.com/alexa.resources/Friends+-+London+Baby!.mp3" />`;
        }

        // Respond to User
        if (cityMatch !== '') {
            if (cityOrganisers.length === 1) {
                this.emit(':ask', `${londonCheck} The organiser of the ${city} Alexa developer meetup is ${cityOrganisers[0]}.`, 'How can i help?');
            } else { // Multiple Organisers
                this.emit(':ask', `${londonCheck} The organisers of the ${city} Alexa developer meetup are: ${convertArrayToReadableString(cityOrganisers)}`, 'How can i help?');
            }
        } else {
            this.emit(':ask', `Sorry, looks like ${city} doesn't have an Alexa developer meetup yet - why don't you start one!`, 'How can i help?');
        }
    },

    'AMAZON.StopIntent': function () {
        //The `tell` action saves the session automatically into the DynamoDB table.
        this.emit(':tell', 'Goodbye!');
    },

    'AMAZON.CancelIntent': function () {
        //The `tell` action saves the session automatically into the DynamoDB table.
        this.emit(':tell', 'Goodbye!');
    },

    'AMAZON.HelpIntent': function () {
        this.emit(':ask', `You can ask me about the various alexa meetups around the world, or listen to the alexa dev chat podcast.`, `What would you like to do?`);
    },
    
    // This handler is invoked when user does not respond back. 
    'SessionEndRequest': function () {
        // Forcefully handles saving the contents of this.attributes and the current handler state to DynamoDB and 
        // then sends the previously built response to the Alexa service. 
        this.emit(':saveState', true);
    },
    
    'Unhandled': function () {
        this.emitWithState('AMAZON.HelpIntent');
    }
});

module.exports = mainStateHandlers;