var Alexa = require('alexa-sdk');
var constants = require('../constants/constants');

var onboardingStateHandlers = Alexa.CreateStateHandler(constants.states.ONBOARDING, {
    'NewSession': function () {
        // Get user name from session - Every amazon Alexa has a unique but anonymous id. By hooking up a DynamoDB table, the Alexa SDK
        // saves the user information and any session information into the DynamoDB table. When a new session is invoked
        // the Alexa SDK looks for a row in the DynamoDB for that specific alexa user account. If it finds a record
        // in the DynamoDB table, it will pull all session information from that DynamoDB table.
        var userName = this.attributes[`userName`];

        if (userName) {
            this.handler.state = constants.states.MAIN;
            this.emitWithState('LaunchRequest');
        } else {
            this.emit(':ask', 'Welcome to Alexa Meetup! The skill that gives you all the information about the alexa developer community. You can ask me about the various alexa meetups around the world, or listen to the alexa dev chat podcast. But first, I\'d like to get to know you better. Tell me your name by saying: My name is, and then your name.', 'Tell me your name by saying: My name is, and then your name.');
        }
    },

    'NameCapture': function () {
        var name;
        var USFirstNameSlot = this.event.request.intent.slots.USFirstName.value;
        var UKFirstNameSlot = this.event.request.intent.slots.UKFirstName.value;

        if (USFirstNameSlot) {
            name = USFirstNameSlot;
        } else if (UKFirstNameSlot) {
            name = UKFirstNameSlot;
        }

        if (name) {
            this.attributes['userName'] = name;
            this.emit(':ask', `Ok ${name}! Please tell me what country you're from by saying: I am from, and then the country you're from.`, `Tell me what country you're from by saying: I am from, and then the country you're from.`)
        } else {
            this.emit(':ask', `Sorry, I didn't recognise that name!`, `Tell me your name by saying: My name is, and then your name.`)
        }
    },

    CountryCapture: function () {
        //Get country value
        var userCountry = this.event.request.intent.slots.Country.value;

        //Get user name from session attributes
        var userName = this.attributes.userName;

        if (userCountry) {
            this.attributes[`userCountry`] = userCountry;
            this.handler.state = constants.states.MAIN;
            this.emit(':ask', `Ok ${userName}! You are from ${userCountry}, that's great! You can ask me about the various alexa meetups around the world and in ${userCountry}, or listen to the alexa dev chat podcast.  What would you like to do?`, `What would you like to do?`);
        } else {
            this.emit(':ask', `Sorry ${name}, I didn't recognise that country! Tell me what country you're from by saying: I am from, and then the country you're from.`, `Tell me what country you're from by saying: I am from, and then the country you're from.`);
        }
    },

    'AMAZON.StopIntent': function () {
        //The `tell` action saves the session automatically into the DynamoDB table.
        this.emit(':tell', 'Goodbye!');
    },

    'AMAZON.CancelIntent': function () {
        //The `tell` action saves the session automatically into the DynamoDB table.
        this.emit(':tell', 'Goodbye!');
    },

    'AMAZON.HelpIntent': function () {
        var userName = this.attributes[`userName`];

        if (userName) {
            this.emit(':ask', `Please tell me what country you're from by saying: I am from, and then the country you're from.`, `Tell me what country you're from by saying: I am from, and then the country you're from.`);
        } else {
            this.emit(':ask', `Please tell me your name by saying: My name is, and then your name.`, `Tell me your name by saying: My name is, and then your name.`);
        }
    },

    // This handler is invoked when user does not respond back. 
    'SessionEndRequest': function () {
        // Forcefully handles saving the contents of this.attributes and the current handler state to DynamoDB and 
        // then sends the previously built response to the Alexa service. 
        this.emit(':saveState', true);
    },
    
    'Unhandled': function () {
        this.emitWithState('AMAZON.HelpIntent');
    }
});

module.exports = onboardingStateHandlers;