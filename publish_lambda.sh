rm lambda.zip
cd lambda
npm install
zip -r ../lambda.zip *
cd ..
aws lambda update-function-code --function-name alexaMeetups --zip-file fileb://lambda.zip
# aws lambda update-function-code --function-name alexaHelloWorld --zip-file fileb://lambda.zip